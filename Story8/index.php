<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Namaku Dito</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="js/jquery-3.5.0.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#accordion" ).accordion();
  } );
  </script>
</head>
<body>
 
<div id="accordion">
  <h3>Nama</h3>
  <div>
    <p>
        Namaku Fauzan Hanandito.
    </p>
  </div>
  <h3>Universitas</h3>
  <div>
    <p>
        Saat ini berkuliah di Universitas Indonesia jurusan ilmu komputer.
    </p>
  </div>
  <h3>Hobi</h3>
  <div>
    <p>
        Hobiku adalah mengoding, nonton YouTube, chat di Discord, dan main game.
    </p>
  </div>
</div>
</body>
</html>